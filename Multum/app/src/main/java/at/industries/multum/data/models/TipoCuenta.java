package at.industries.multum.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TipoCuenta implements Serializable {

    @SerializedName("id_tipo_cuenta")
    private Integer idTipoCuenta;
    @SerializedName("usuario")
    private String usuario;
    @SerializedName("id_entidad")
    private Integer idEntidad;
    @SerializedName("id_estatus")
    private Integer idEstatus;
    @SerializedName("clave")
    private Integer clave;
    @SerializedName("tipo_cuenta")
    private String tipoCuenta;
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;

    public Integer getIdTipoCuenta() {
        return idTipoCuenta;
    }

    public void setIdTipoCuenta(Integer idTipoCuenta) {
        this.idTipoCuenta = idTipoCuenta;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(Integer idEntidad) {
        this.idEntidad = idEntidad;
    }

    public Integer getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(Integer idEstatus) {
        this.idEstatus = idEstatus;
    }

    public Integer getClave() {
        return clave;
    }

    public void setClave(Integer clave) {
        this.clave = clave;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return clave + " - " + tipoCuenta;
    }
}
