package at.industries.multum.ui.movimientos;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import at.industries.multum.R;
import at.industries.multum.data.base.RetrofitHandler;
import at.industries.multum.data.base.SessionPrefs;
import at.industries.multum.data.models.Cuenta;
import at.industries.multum.data.models.Estatus;
import at.industries.multum.data.models.Movimiento;
import at.industries.multum.ui.movimientos.ListFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditFragment extends Fragment {

    private String usuario;
    private Integer idEntidad;
    Context context;
    Spinner cuentasCombo;
    BottomNavigationView navigationView;
    private final String pattern = "dd/MM/yyyy";

    private static final String DESCRIBABLE_KEY = "movimiento";
    private Movimiento movimiento;

    public static EditFragment newInstance(Movimiento movimiento) {
        EditFragment fragment = new EditFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DESCRIBABLE_KEY, movimiento);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();
        usuario = SessionPrefs.get(context).getUsuario();
        idEntidad = SessionPrefs.get(context).getIdEntidad();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            movimiento = (Movimiento) getArguments().getSerializable(DESCRIBABLE_KEY);
        } catch (Exception e) {
            e.printStackTrace();
            movimiento = null;
        }
        return inflater.inflate(R.layout.fragment_movimientos_edit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navigationView = getActivity().findViewById(R.id.bottom_nav);

        final Button saveButton = getActivity().findViewById(R.id.save);
        final Button deleteButton = getActivity().findViewById(R.id.delete);

        final EditText idInput = getActivity().findViewById(R.id.idMovimiento);
        final EditText fechaInput = getActivity().findViewById(R.id.fechaInput);
        // TODO  Implement date picker //
        final EditText descripcionInput = getActivity().findViewById(R.id.descripcionInput);
        final EditText importeInput = getActivity().findViewById(R.id.importeInput);
        final EditText tipoInput = getActivity().findViewById(R.id.tipoInput);

        final TextView labelCrearMovimiento = getActivity().findViewById(R.id.labelCrearMovimiento);

        cuentasCombo = getActivity().findViewById(R.id.cuentas_combo);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
                Cuenta cuenta = (Cuenta) cuentasCombo.getSelectedItem();
                Movimiento ins = new Movimiento();
                Date date;
                try {
                    ins.setIdMovimiento(Integer.parseInt(idInput.getText().toString()));
                    date = new SimpleDateFormat(pattern).parse(fechaInput.getText().toString());
                } catch (Exception e) {
                    ins.setIdMovimiento(null);
                    date = new Date();
                }
                ins.setIdCuenta(cuenta.getIdCuenta());
                ins.setIdEntidad(idEntidad);
                ins.setUsuario(usuario);
                // TODO get date picker value //
                ins.setFecha(date);
                ins.setDescripcion(descripcionInput.getText().toString());
                ins.setImporte(Double.parseDouble(importeInput.getText().toString()));
                ins.setTipo(tipoInput.getText().toString());
                Call<Movimiento> call1 = RetrofitHandler.getInstance(context).getMyApi().insertMovimiento(ins, SessionPrefs.get(context).getToken());
                call1.enqueue(new Callback<Movimiento>() {

                    @Override
                    public void onResponse(Call<Movimiento> call, Response<Movimiento> response) {
                        loading.dismiss();
                        Movimiento movimiento = response.body();
                        Toast.makeText(context, movimiento.getMessage(), Toast.LENGTH_LONG).show();
                        navigationView.setSelectedItemId(R.id.action_list);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListFragment()).commit();
                    }

                    @Override
                    public void onFailure(Call<Movimiento> call, Throwable t) {
                        Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
                    }

                });
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Movimiento ins = new Movimiento();
                ins.setDescripcion(descripcionInput.getText().toString());
                ins.setIdMovimiento(Integer.parseInt(idInput.getText().toString()));

                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Borrar movimiento")
                        .setMessage("¿Está seguro que desea eliminar \"" + ins.getDescripcion() + "\" ?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
                                Call<Movimiento> call1 = RetrofitHandler.getInstance(context).getMyApi().deleteMovimiento(ins.getIdMovimiento(), SessionPrefs.get(context).getToken());
                                call1.enqueue(new Callback<Movimiento>() {

                                    @Override
                                    public void onResponse(Call<Movimiento> call, Response<Movimiento> response) {
                                        loading.dismiss();
                                        Movimiento movimiento = response.body();
                                        Toast.makeText(context, movimiento.getMessage(), Toast.LENGTH_LONG).show();
                                        navigationView.setSelectedItemId(R.id.action_list);
                                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListFragment()).commit();
                                    }

                                    @Override
                                    public void onFailure(Call<Movimiento> call, Throwable t) {
                                        Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
                                    }

                                });
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
        Call<List<Cuenta>> call1 = RetrofitHandler.getInstance(context).getMyApi().getCuentas(SessionPrefs.get(context).getIdEntidad());
        call1.enqueue(new Callback<List<Cuenta>>() {

            @Override
            public void onResponse(Call<List<Cuenta>> call, Response<List<Cuenta>> response) {
                loading.dismiss();
                List<Cuenta> cuentas = response.body();
                ArrayAdapter<Cuenta> cuentaAdapter = new ArrayAdapter<Cuenta>(context, android.R.layout.simple_spinner_item, cuentas);
                cuentaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                cuentasCombo.setAdapter(cuentaAdapter);
                if (movimiento != null) {
                    idInput.setText(movimiento.getIdMovimiento().toString());
                    selectSpinnerItemByValue(cuentasCombo, movimiento.getIdCuenta());
                    // TODO charge fecha value on date picker //
                    descripcionInput.setText(movimiento.getDescripcion());
                    importeInput.setText(movimiento.getImporte().toString());
                    tipoInput.setText(movimiento.getTipo());
                    fechaInput.setText(new SimpleDateFormat(pattern).format(movimiento.getFecha()));
                    deleteButton.setEnabled(true);
                    labelCrearMovimiento.setText("Modo edición");
                } else {
                    labelCrearMovimiento.setText("Modo creación");
                }
            }

            @Override
            public void onFailure(Call<List<Cuenta>> call, Throwable t) {
                Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
            }

        });
    }

    public static void selectSpinnerItemByValue(Spinner spnr, long value) {
        Adapter adapter = spnr.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            Cuenta cuenta = (Cuenta) adapter.getItem(position);
            if (cuenta.getIdCuenta() == value) {
                spnr.setSelection(position);
                return;
            }
        }
    }

}
