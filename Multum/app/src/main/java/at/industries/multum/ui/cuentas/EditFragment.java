package at.industries.multum.ui.cuentas;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import at.industries.multum.R;
import at.industries.multum.data.base.RetrofitHandler;
import at.industries.multum.data.base.SessionPrefs;
import at.industries.multum.data.models.Cuenta;
import at.industries.multum.data.models.Estatus;
import at.industries.multum.data.models.TipoCuenta;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditFragment extends Fragment {

    private String usuario;
    private Integer idEntidad;
    Context context;
    Spinner estatusCombo;
    Spinner tiposCuentasCombo;
    BottomNavigationView navigationView;

    private static final String DESCRIBABLE_KEY = "cuenta";
    private Cuenta cuenta;

    public static EditFragment newInstance(Cuenta cuenta) {
        EditFragment fragment = new EditFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DESCRIBABLE_KEY, cuenta);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();
        usuario = SessionPrefs.get(context).getUsuario();
        idEntidad = SessionPrefs.get(context).getIdEntidad();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            cuenta = (Cuenta) getArguments().getSerializable(DESCRIBABLE_KEY);
        } catch (Exception e) {
            e.printStackTrace();
            cuenta = null;
        }
        return inflater.inflate(R.layout.fragment_cuentas_edit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navigationView = getActivity().findViewById(R.id.bottom_nav);

        final Button saveButton = getActivity().findViewById(R.id.save);
        final Button deleteButton = getActivity().findViewById(R.id.delete);

        final EditText idInput = getActivity().findViewById(R.id.idCuenta);
        final EditText claveInput = getActivity().findViewById(R.id.claveInput);
        final EditText cuentaInput = getActivity().findViewById(R.id.cuentaInput);
        final EditText ordenInput = getActivity().findViewById(R.id.ordenInput);

        final TextView labelCrearCuenta = getActivity().findViewById(R.id.labelCrearCuenta);

        estatusCombo = getActivity().findViewById(R.id.estatus_combo);
        tiposCuentasCombo = getActivity().findViewById(R.id.tipos_cuentas_combo);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
                Estatus estatus = (Estatus) estatusCombo.getSelectedItem();
                TipoCuenta tipoCuenta = (TipoCuenta) tiposCuentasCombo.getSelectedItem();
                Cuenta ins = new Cuenta();
                try {
                    ins.setIdCuenta(Integer.parseInt(idInput.getText().toString()));
                } catch (Exception e) {
                    ins.setIdCuenta(null);
                }
                ins.setCuenta(cuentaInput.getText().toString());
                Log.d("Debug", idEntidad.toString());
                ins.setIdEntidad(idEntidad);
                ins.setIdEstatus(estatus.getIdEstatus());
                ins.setIdTipoCuenta(tipoCuenta.getIdTipoCuenta());
                ins.setUsuario(usuario);
                ins.setClave(claveInput.getText().toString());
                ins.setOrden(Integer.parseInt(ordenInput.getText().toString()));
                Call<Cuenta> call1 = RetrofitHandler.getInstance(context).getMyApi().insertCuenta(ins, SessionPrefs.get(context).getToken());
                call1.enqueue(new Callback<Cuenta>() {

                    @Override
                    public void onResponse(Call<Cuenta> call, Response<Cuenta> response) {
                        loading.dismiss();
                        Cuenta cuenta = response.body();
                        Toast.makeText(context, cuenta.getMessage(), Toast.LENGTH_LONG).show();
                        navigationView.setSelectedItemId(R.id.action_list);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListFragment()).commit();
                    }

                    @Override
                    public void onFailure(Call<Cuenta> call, Throwable t) {
                        Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
                    }

                });
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cuenta ins = new Cuenta();
                ins.setCuenta(cuentaInput.getText().toString());
                ins.setIdCuenta(Integer.parseInt(idInput.getText().toString()));

                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Borrar cuenta")
                        .setMessage("¿Está seguro que desea eliminar \"" + ins.getCuenta() + "\" ?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
                                Call<Cuenta> call1 = RetrofitHandler.getInstance(context).getMyApi().deleteCuenta(ins.getIdCuenta(), SessionPrefs.get(context).getToken());
                                call1.enqueue(new Callback<Cuenta>() {

                                    @Override
                                    public void onResponse(Call<Cuenta> call, Response<Cuenta> response) {
                                        loading.dismiss();
                                        Cuenta cuenta = response.body();
                                        Toast.makeText(context, cuenta.getMessage(), Toast.LENGTH_LONG).show();
                                        navigationView.setSelectedItemId(R.id.action_list);
                                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListFragment()).commit();
                                    }

                                    @Override
                                    public void onFailure(Call<Cuenta> call, Throwable t) {
                                        Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
                                    }

                                });
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
        Call<List<Estatus>> call1 = RetrofitHandler.getInstance(context).getMyApi().getEstatus(SessionPrefs.get(context).getIdEntidad());
        call1.enqueue(new Callback<List<Estatus>>() {

            @Override
            public void onResponse(Call<List<Estatus>> call, Response<List<Estatus>> response) {
                List<Estatus> estatus = response.body();
                ArrayAdapter<Estatus> estatusAdapter = new ArrayAdapter<Estatus>(context, android.R.layout.simple_spinner_item, estatus);
                estatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                estatusCombo.setAdapter(estatusAdapter);
                Call<List<TipoCuenta>> call2 = RetrofitHandler.getInstance(context).getMyApi().getTiposCuentas(SessionPrefs.get(context).getIdEntidad());
                call2.enqueue(new Callback<List<TipoCuenta>>() {

                    @Override
                    public void onResponse(Call<List<TipoCuenta>> call, Response<List<TipoCuenta>> response) {
                        loading.dismiss();
                        List<TipoCuenta> tiposCuentas = response.body();
                        ArrayAdapter<TipoCuenta> tiposCuentasAdapter = new ArrayAdapter<TipoCuenta>(context, android.R.layout.simple_spinner_item, tiposCuentas);
                        tiposCuentasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        tiposCuentasCombo.setAdapter(tiposCuentasAdapter);
                        if (cuenta != null) {
                            idInput.setText(cuenta.getIdCuenta().toString());
                            selectEstatusItemByValue(estatusCombo, cuenta.getIdEstatus());
                            selectTipoCuentaItemByValue(tiposCuentasCombo, cuenta.getIdTipoCuenta());
                            claveInput.setText(cuenta.getClave());
                            cuentaInput.setText(cuenta.getCuenta());
                            ordenInput.setText(cuenta.getOrden().toString());
                            deleteButton.setEnabled(true);
                            labelCrearCuenta.setText("Modo edición");
                        } else {
                            labelCrearCuenta.setText("Modo creación");
                        }
                    }

                    @Override
                    public void onFailure(Call<List<TipoCuenta>> call, Throwable t) {
                        Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
                    }

                });
            }

            @Override
            public void onFailure(Call<List<Estatus>> call, Throwable t) {
                Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
            }

        });
    }

    public static void selectEstatusItemByValue(Spinner spnr, long value) {
        Adapter adapter = spnr.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            Estatus estatus = (Estatus) adapter.getItem(position);
            if (estatus.getIdEstatus() == value) {
                spnr.setSelection(position);
                return;
            }
        }
    }

    public static void selectTipoCuentaItemByValue(Spinner spnr, long value) {
        Adapter adapter = spnr.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            TipoCuenta tipoCuenta = (TipoCuenta) adapter.getItem(position);
            if (tipoCuenta.getIdTipoCuenta() == value) {
                spnr.setSelection(position);
                return;
            }
        }
    }
}
