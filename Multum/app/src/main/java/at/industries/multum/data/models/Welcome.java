package at.industries.multum.data.models;

import com.google.gson.annotations.SerializedName;

public class Welcome {

    @SerializedName("entradas")
    private Double entradas;
    @SerializedName("salidas")
    private Double salidas;

    public Double getEntradas() {
        return entradas;
    }

    public void setEntradas(Double entradas) {
        this.entradas = entradas;
    }

    public Double getSalidas() {
        return salidas;
    }

    public void setSalidas(Double salidas) {
        this.salidas = salidas;
    }
}
