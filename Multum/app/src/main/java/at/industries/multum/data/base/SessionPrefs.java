package at.industries.multum.data.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import at.industries.multum.data.models.User;

public class SessionPrefs {

    private static SessionPrefs INSTANCE;
    private final SharedPreferences mPrefs;
    private boolean mIsLoggedIn = false;

    public static final String PREFS_NAME = "MULTUM_PREFS";
    public static final String PREF_USER = "PREF_USER";
    public static final String PREF_USER_NAME = "PREF_USER_NAME";
    public static final String PREF_USER_TOKEN = "PREF_USER_TOKEN";
    public static final String PREF_USER_ID_ENTIDAD = "PREF_USER_ID_ENTIDAD";
    public static final String PREF_USER_ENTIDAD = "PREF_USER_ENTIDAD";
    public static final String PREF_USER_MAIL = "PREF_USER_MAIL";
    public static final String PREF_USER_SUPER = "PREF_USER_SUPER";

    public static SessionPrefs get(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new SessionPrefs(context);
        }
        return INSTANCE;
    }

    private SessionPrefs(Context context) {
        mPrefs = context.getApplicationContext()
                .getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        mIsLoggedIn = !TextUtils.isEmpty(mPrefs.getString(PREF_USER_TOKEN, null));
    }

    public boolean isLoggedIn(){
        return mIsLoggedIn;
    }

    public void setToken(String token){
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_USER_TOKEN, token);
        editor.apply();
    }

    public String getToken(){
        return mPrefs.getString(PREF_USER_TOKEN, null);
    }

    public Integer getIdEntidad(){ return mPrefs.getInt(PREF_USER_ID_ENTIDAD, 0); }

    public String getUsuario(){ return mPrefs.getString(PREF_USER, null);}

    public String getEntidad(){ return mPrefs.getString(PREF_USER_ENTIDAD, null);}

    public User getActualUser(Context context){
        SharedPreferences prefs = context.getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String id = prefs.getString(PREF_USER, null);
        String displayName = prefs.getString(PREF_USER_NAME, null);
        String token = prefs.getString(PREF_USER_TOKEN, null);
        Integer idEntidad = prefs.getInt(PREF_USER_ID_ENTIDAD, 0);
        String entidad = prefs.getString(PREF_USER_ENTIDAD, null);
        String email = prefs.getString(PREF_USER_MAIL, null);
        Integer superUser = prefs.getInt(PREF_USER_SUPER, 0);

        return new User(id, displayName, token, idEntidad, entidad, email, superUser);
    }

    public void saveLogin(User user) {
        if (user.getToken() != null) {
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString(PREF_USER, user.getUsuario());
            editor.putString(PREF_USER_NAME, user.getDisplayName());
            editor.putString(PREF_USER_TOKEN, user.getToken());
            editor.putInt(PREF_USER_ID_ENTIDAD, user.getIdEntidad());
            editor.putString(PREF_USER_ENTIDAD, user.getEntidad());
            editor.putString(PREF_USER_MAIL, user.getEmail());
            editor.putInt(PREF_USER_SUPER, user.getSuperUser());
            editor.apply();

            mIsLoggedIn = true;
        }
    }

    public void logOut(){
        mIsLoggedIn = false;
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_USER, null);
        editor.putString(PREF_USER_NAME, null);
        editor.putString(PREF_USER_TOKEN, null);
        editor.putInt(PREF_USER_ID_ENTIDAD, 0);
        editor.putString(PREF_USER_ENTIDAD, null);
        editor.putString(PREF_USER_MAIL, null);
        editor.putInt(PREF_USER_SUPER, 0);
        editor.apply();
    }
}
