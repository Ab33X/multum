package at.industries.multum.ui.tipos_cuentas;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import at.industries.multum.R;
import at.industries.multum.data.base.RetrofitHandler;
import at.industries.multum.data.base.SessionPrefs;
import at.industries.multum.data.models.Estatus;
import at.industries.multum.data.models.TipoCuenta;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListFragment extends Fragment {
    // TODO: 10/04/2021 Handle tipo cuenta list empty. With dummy or guided creation
    ListView tiposCuentasList;
    BottomNavigationView navigationView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tipos_cuentas_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navigationView = getActivity().findViewById(R.id.bottom_nav);

        tiposCuentasList = getActivity().findViewById(R.id.tipos_cuentas_list);
        tiposCuentasList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TipoCuenta item = (TipoCuenta) parent.getItemAtPosition(position);
                navigationView.setSelectedItemId(R.id.action_edit);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, EditFragment.newInstance(item)).commit();

            }

        });

        Context context = getActivity().getApplicationContext();

        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
        Call<List<TipoCuenta>> call2 = RetrofitHandler.getInstance(context).getMyApi().getTiposCuentas(SessionPrefs.get(context).getIdEntidad());
        call2.enqueue(new Callback<List<TipoCuenta>>() {

            @Override
            public void onResponse(Call<List<TipoCuenta>> call, Response<List<TipoCuenta>> response) {
                loading.dismiss();
                List<TipoCuenta> tiposCuentas = response.body();
                ArrayAdapter<TipoCuenta> tiposCuentasAdapter = new ArrayAdapter<TipoCuenta>(context,  R.layout.simple_item, tiposCuentas);
                tiposCuentasList.setAdapter(tiposCuentasAdapter);
                tiposCuentasAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<TipoCuenta>> call, Throwable t) {
                Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
            }

        });
    }
}