package at.industries.multum.ui.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.util.List;

import at.industries.multum.R;
import at.industries.multum.data.base.RetrofitHandler;
import at.industries.multum.data.base.SessionPrefs;
import at.industries.multum.data.models.Cuenta;
import at.industries.multum.data.models.Welcome;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    Context context;
    String entidad;
    TextView entidadTxtVw;
    TextView entradasTxtVw;
    TextView salidasTxtVw;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        context = getActivity().getApplicationContext();
        if (SessionPrefs.get(context).getEntidad() != null) {
            this.entidad = SessionPrefs.get(context).getEntidad();
        } else {
            this.entidad = "Multum";
        }
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        entidadTxtVw = getActivity().findViewById(R.id.entidad_txt_vw);
        entradasTxtVw = getActivity().findViewById(R.id.entradas_txt_vw);
        salidasTxtVw = getActivity().findViewById(R.id.salidas_txt_vw);

        entidadTxtVw.setText(entidad);
        Context context = getActivity().getApplicationContext();

        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
        Call<Welcome> call1 = RetrofitHandler.getInstance(context).getMyApi().getWelcome(SessionPrefs.get(context).getIdEntidad());
        call1.enqueue(new Callback<Welcome>() {

            @Override
            public void onResponse(Call<Welcome> call, Response<Welcome> response) {
                loading.dismiss();
                Welcome welcome = response.body();
                entradasTxtVw.setText("Entradas: " + welcome.getEntradas());
                salidasTxtVw.setText("Salidas:" + welcome.getSalidas());
            }

            @Override
            public void onFailure(Call<Welcome> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
            }

        });
    }
}