package at.industries.multum.data.models;


import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class User {

    @SerializedName("id_usuario")
    private Integer idUsuario;
    @SerializedName("usuario")
    private String usuario;
    @SerializedName("email")
    private String email;
    @SerializedName("nombre")
    private String displayName;
    @SerializedName("message")
    private String message;
    @SerializedName("token")
    private String token;
    @SerializedName("id_entidad")
    private Integer idEntidad;
    @SerializedName("entidad")
    @JsonAdapter(EntidadDeserializer.class)
    private String entidad;
    @SerializedName("super")
    private Integer superUser;

    public User(String usuario, String displayName, String message, String token) {
        this.usuario = usuario;
        this.displayName = displayName;
        this.message = message;
        this.token = token;
    }

    public User(String usuario, String displayName, String token, Integer idEntidad, String entidad, String email, Integer superUser) {
        this.usuario = usuario;
        this.displayName = displayName;
        this.idEntidad = idEntidad;
        this.entidad = entidad;
        this.token = token;
        this.email = email;
        this.superUser = superUser;
    }

    public User(String message) {
        this.message = message;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }

    public Integer getIdEntidad() {
        return idEntidad;
    }

    public Integer getSuperUser() {
        return superUser;
    }

    public String getEmail() {
        return email;
    }

    public String getEntidad() {return entidad;}

    public static class EntidadDeserializer implements JsonDeserializer<String> {
        @Override
        public String deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
            return json.getAsJsonObject().get("entidad").getAsString();
        }
    }
}