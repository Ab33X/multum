package at.industries.multum.data.models;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;

public class Estatus {

    @SerializedName("id_estatus")
    private Integer idEstatus;
    @SerializedName("estatus")
    private String estatus;
    @SerializedName("usuario")
    private String usuario;
    @SerializedName("id_entidad")
    private Integer idEntidad;
    @SerializedName("id_tipo_estatus")
    private Integer idTipoEstatus;
    @SerializedName("tipo_estatus")
    @JsonAdapter(Estatus.TipoEstatusDeserializer.class)
    private String entidad;

    public Integer getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(Integer idEstatus) {
        this.idEstatus = idEstatus;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(Integer idEntidad) {
        this.idEntidad = idEntidad;
    }

    public Integer getIdTipoEstatus() {
        return idTipoEstatus;
    }

    public void setIdTipoEstatus(Integer idTipoEstatus) {
        this.idTipoEstatus = idTipoEstatus;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public static class TipoEstatusDeserializer implements JsonDeserializer<String> {
        @Override
        public String deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
            return json.getAsJsonObject().get("tipo_estatus").getAsString();
        }
    }

    @Override
    public String toString() {
        return estatus;
    }
}
