package at.industries.multum.data.models;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Type;

public class Cuenta implements Serializable {

    @SerializedName("id_cuenta")
    private Integer idCuenta;
    @SerializedName("usuario")
    private String usuario;
    @SerializedName("id_entidad")
    private Integer idEntidad;
    @SerializedName("id_estatus")
    private Integer idEstatus;
    @SerializedName("id_tipo_cuenta")
    private Integer idTipoCuenta;
    @SerializedName("clave")
    private String clave;
    @SerializedName("cuenta")
    private String cuenta;
    @SerializedName("orden")
    private Integer orden;
    @SerializedName("tipo_cuenta")
    @JsonAdapter(Cuenta.TipoCuentaDeserializer.class)
    private String tipoCuenta;
    @SerializedName("estatus")
    @JsonAdapter(Cuenta.EstatusDeserializer.class)
    private String estatus;
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;

    public static class TipoCuentaDeserializer implements JsonDeserializer<String> {
        @Override
        public String deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
            return json.getAsJsonObject().get("tipo_cuenta").getAsString();
        }
    }

    public static class EstatusDeserializer implements JsonDeserializer<String> {
        @Override
        public String deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
            return json.getAsJsonObject().get("estatus").getAsString();
        }
    }

    @Override
    public String toString() {
        return tipoCuenta + " - " + cuenta + " (" + estatus + ")";
    }

    public Integer getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Integer idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(Integer idEntidad) {
        this.idEntidad = idEntidad;
    }

    public Integer getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(Integer idEstatus) {
        this.idEstatus = idEstatus;
    }

    public Integer getIdTipoCuenta() {
        return idTipoCuenta;
    }

    public void setIdTipoCuenta(Integer idTipoCuenta) {
        this.idTipoCuenta = idTipoCuenta;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

