package at.industries.multum.data.models;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Movimiento implements Serializable {

    private final String pattern = "dd/MM/yyyy";

    @SerializedName("id_movimiento")
    private Integer idMovimiento;
    @SerializedName("usuario")
    private String usuario;
    @SerializedName("id_entidad")
    private Integer idEntidad;
    @SerializedName("id_cuenta")
    private Integer idCuenta;
    @SerializedName("fecha")
    private Date fecha;
    @SerializedName("descripcion")
    private String descripcion;
    @SerializedName("importe")
    private Double importe;
    @SerializedName("tipo")
    private String tipo;
    @SerializedName("cuenta")
    @JsonAdapter(Movimiento.CuentaDeserializer.class)
    private String cuenta;
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;

    public static class CuentaDeserializer implements JsonDeserializer<String> {
        @Override
        public String deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
            return json.getAsJsonObject().get("cuenta").getAsString();
        }
    }

    @Override
    public String toString() {
        return new SimpleDateFormat(pattern).format(fecha) + " - " + descripcion + " - $" + importe;
    }

    public Integer getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(Integer idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(Integer idEntidad) {
        this.idEntidad = idEntidad;
    }

    public Integer getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Integer idCuenta) {
        this.idCuenta = idCuenta;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
