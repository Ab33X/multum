package at.industries.multum.data.login;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import at.industries.multum.MainActivity;
import at.industries.multum.data.base.Result;
import at.industries.multum.data.base.RetrofitHandler;
import at.industries.multum.data.base.SessionPrefs;
import at.industries.multum.data.models.ErrorResponse;
import at.industries.multum.data.models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
public class LoginRepository {

    private static volatile LoginRepository instance;
    Result<User> result;

    public static LoginRepository getInstance() {
        if (instance == null) {
            instance = new LoginRepository();
        }
        return instance;
    }

    public void logout(Context context) {
        SessionPrefs.get(context).logOut();
    }

    public void login(Context context, String username, String password) {
        Call<List<User>> call = RetrofitHandler.getInstance(context).getMyApi().getUserLogin(username, password);
        call.enqueue(new Callback<List<User>>() {

            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if(response.isSuccessful()){
                    result = new Result.Success<>(response.body().get(0));
                    User user = ((Result.Success<User>) result).getData();
                    if (result instanceof Result.Success) {
                        if(user.getToken() != null){
                            SessionPrefs.get(context).saveLogin(user);
                            Toast.makeText(context.getApplicationContext(), user.getMessage(),Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(context.getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(intent);
                        } else {
                            Toast.makeText(context.getApplicationContext(), user.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(context.getApplicationContext(), "Request failed", Toast.LENGTH_LONG).show();
            }

        });
    }
}