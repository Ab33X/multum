package at.industries.multum.data.base;

import java.util.List;

import at.industries.multum.data.models.Cuenta;
import at.industries.multum.data.models.Estatus;
import at.industries.multum.data.models.Movimiento;
import at.industries.multum.data.models.TipoCuenta;
import at.industries.multum.data.models.Token;
import at.industries.multum.data.models.User;
import at.industries.multum.data.models.Welcome;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitApi {

    String BASE_URL = "http://192.168.0.7:3000/";

    @GET("token")
    Call<Token> getToken();

    @GET("usuario?")
    Call<List<User>> getUserLogin(@Query("usuario") String user, @Query("password") String password);

    @GET("estatus/{idEntidad}")
    Call<List<Estatus>> getEstatus(@Path("idEntidad") Integer idEntidad);

    @GET("tipos/cuentas/{idEntidad}")
    Call<List<TipoCuenta>> getTiposCuentas(@Path("idEntidad") Integer idEntidad);

    @POST("tipo/cuenta")
    Call<TipoCuenta> insertTipoCuenta(@Body TipoCuenta tipoCuenta, @Header("X-CSRF-TOKEN") String token);

    @DELETE("tipo/cuenta?")
    Call<TipoCuenta> deleteTipoCuenta(@Query("id_tipo_cuenta") Integer idTipoCuenta, @Header("X-CSRF-TOKEN") String token);

    @GET("cuentas/{idEntidad}")
    Call<List<Cuenta>> getCuentas(@Path("idEntidad") Integer idEntidad);

    @POST("cuenta")
    Call<Cuenta> insertCuenta(@Body Cuenta cuenta, @Header("X-CSRF-TOKEN") String token);

    @DELETE("cuenta?")
    Call<Cuenta> deleteCuenta(@Query("id_cuenta") Integer idCuenta, @Header("X-CSRF-TOKEN") String token);

    @GET("movimientos/{idEntidad}")
    Call<List<Movimiento>> getMovimientos(@Path("idEntidad") Integer idEntidad);

    @POST("movimiento")
    Call<Movimiento> insertMovimiento(@Body Movimiento movimiento, @Header("X-CSRF-TOKEN") String token);

    @DELETE("movimiento?")
    Call<Movimiento> deleteMovimiento(@Query("id_movimiento") Integer idMovimiento, @Header("X-CSRF-TOKEN") String token);

    @GET("consultas/{idEntidad}")
    Call<Welcome> getWelcome(@Path("idEntidad") Integer idEntidad);
}
