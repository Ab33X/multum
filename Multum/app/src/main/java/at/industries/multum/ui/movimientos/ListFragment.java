package at.industries.multum.ui.movimientos;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import at.industries.multum.R;
import at.industries.multum.data.base.RetrofitHandler;
import at.industries.multum.data.base.SessionPrefs;
import at.industries.multum.data.models.Movimiento;
import at.industries.multum.ui.movimientos.EditFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListFragment extends Fragment {

    ListView movimientosList;
    BottomNavigationView navigationView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movimientos_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navigationView = getActivity().findViewById(R.id.bottom_nav);

        movimientosList = getActivity().findViewById(R.id.movimientos_list);
        movimientosList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Movimiento item = (Movimiento) parent.getItemAtPosition(position);
                navigationView.setSelectedItemId(R.id.action_edit);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, EditFragment.newInstance(item)).commit();

            }

        });

        Context context = getActivity().getApplicationContext();

        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
        Call<List<Movimiento>> call2 = RetrofitHandler.getInstance(context).getMyApi().getMovimientos(SessionPrefs.get(context).getIdEntidad());
        call2.enqueue(new Callback<List<Movimiento>>() {

            @Override
            public void onResponse(Call<List<Movimiento>> call, Response<List<Movimiento>> response) {
                loading.dismiss();
                List<Movimiento> movimientos = response.body();
                ArrayAdapter<Movimiento> movimientosAdapter = new ArrayAdapter<Movimiento>(context, R.layout.simple_item, movimientos);
                movimientosList.setAdapter(movimientosAdapter);
                movimientosAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Movimiento>> call, Throwable t) {
                Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
            }

        });
    }
}
