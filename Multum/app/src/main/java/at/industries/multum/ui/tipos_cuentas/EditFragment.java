package at.industries.multum.ui.tipos_cuentas;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.Serializable;
import java.util.List;

import at.industries.multum.R;
import at.industries.multum.data.base.RetrofitHandler;
import at.industries.multum.data.base.SessionPrefs;
import at.industries.multum.data.login.LoginRepository;
import at.industries.multum.data.models.Estatus;
import at.industries.multum.data.models.TipoCuenta;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditFragment extends Fragment {

    private String usuario;
    private Integer idEntidad;
    Context context;
    Spinner estatusCombo;
    BottomNavigationView navigationView;

    private static final String DESCRIBABLE_KEY = "tipoCuenta";
    private TipoCuenta tipoCuenta;

    public static EditFragment newInstance(TipoCuenta tipoCuenta) {
        EditFragment fragment = new EditFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DESCRIBABLE_KEY, tipoCuenta);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();
        usuario = SessionPrefs.get(context).getUsuario();
        idEntidad = SessionPrefs.get(context).getIdEntidad();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try{
            tipoCuenta = (TipoCuenta) getArguments().getSerializable(DESCRIBABLE_KEY);
        } catch(Exception e){
            e.printStackTrace();
            tipoCuenta = null;
        }
        return inflater.inflate(R.layout.fragment_tipos_cuentas_edit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navigationView = getActivity().findViewById(R.id.bottom_nav);

        final Button saveButton = getActivity().findViewById(R.id.save);
        final Button deleteButton = getActivity().findViewById(R.id.delete);
        final EditText idInput = getActivity().findViewById(R.id.idTipoCuenta);
        final EditText claveInput = getActivity().findViewById(R.id.claveInput);
        final EditText tipoCuentaInput = getActivity().findViewById(R.id.tipoCuentaInput);
        final TextView labelCrearTipoCuenta = getActivity().findViewById(R.id.labelCrearTipoCuenta);
        estatusCombo = getActivity().findViewById(R.id.estatus_combo);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
                Estatus estatus = (Estatus) estatusCombo.getSelectedItem();
                TipoCuenta ins = new TipoCuenta();
                try{
                    ins.setIdTipoCuenta(Integer.parseInt(idInput.getText().toString()));
                }catch(Exception e){
                    ins.setIdTipoCuenta(null);
                }
                ins.setTipoCuenta(tipoCuentaInput.getText().toString());
                ins.setIdEntidad(idEntidad);
                ins.setIdEstatus(estatus.getIdEstatus());
                ins.setUsuario(usuario);
                ins.setClave(Integer.parseInt(claveInput.getText().toString()));
                Call<TipoCuenta> call1 = RetrofitHandler.getInstance(context).getMyApi().insertTipoCuenta(ins, SessionPrefs.get(context).getToken());
                call1.enqueue(new Callback<TipoCuenta>() {

                    @Override
                    public void onResponse(Call<TipoCuenta> call, Response<TipoCuenta> response) {
                        loading.dismiss();
                        TipoCuenta tipoCuenta = response.body();
                        Toast.makeText(context, tipoCuenta.getMessage(), Toast.LENGTH_LONG).show();
                        navigationView.setSelectedItemId(R.id.action_list);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListFragment()).commit();
                    }

                    @Override
                    public void onFailure(Call<TipoCuenta> call, Throwable t) {
                        Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
                    }

                });
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TipoCuenta ins = new TipoCuenta();
                ins.setTipoCuenta(tipoCuentaInput.getText().toString());
                ins.setIdTipoCuenta(Integer.parseInt(idInput.getText().toString()));

                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Borrar tipo de cuenta")
                        .setMessage("¿Está seguro que desea eliminar \"" + ins.getTipoCuenta() + "\" ?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
                                Call<TipoCuenta> call1 = RetrofitHandler.getInstance(context).getMyApi().deleteTipoCuenta(ins.getIdTipoCuenta(), SessionPrefs.get(context).getToken());
                                call1.enqueue(new Callback<TipoCuenta>() {

                                    @Override
                                    public void onResponse(Call<TipoCuenta> call, Response<TipoCuenta> response) {
                                        loading.dismiss();
                                        TipoCuenta tipoCuenta = response.body();
                                        Toast.makeText(context, tipoCuenta.getMessage(), Toast.LENGTH_LONG).show();
                                        navigationView.setSelectedItemId(R.id.action_list);
                                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListFragment()).commit();
                                    }

                                    @Override
                                    public void onFailure(Call<TipoCuenta> call, Throwable t) {
                                        Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
                                    }

                                });
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Cargando...", "Espere por favor...", false, false);
        Call<List<Estatus>> call1 = RetrofitHandler.getInstance(context).getMyApi().getEstatus(SessionPrefs.get(context).getIdEntidad());
        call1.enqueue(new Callback<List<Estatus>>() {

            @Override
            public void onResponse(Call<List<Estatus>> call, Response<List<Estatus>> response) {
                loading.dismiss();
                List<Estatus> estatus = response.body();
                ArrayAdapter<Estatus> estatusAdapter = new ArrayAdapter<Estatus>(context, android.R.layout.simple_spinner_item, estatus);
                estatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                estatusCombo.setAdapter(estatusAdapter);

                if(tipoCuenta != null){
                    idInput.setText(tipoCuenta.getIdTipoCuenta().toString());
                    selectSpinnerItemByValue(estatusCombo, tipoCuenta.getIdEstatus());
                    claveInput.setText(tipoCuenta.getClave().toString());
                    tipoCuentaInput.setText(tipoCuenta.getTipoCuenta());
                    deleteButton.setEnabled(true);
                    labelCrearTipoCuenta.setText("Modo edición");
                } else {
                    labelCrearTipoCuenta.setText("Modo creación");
                }
            }

            @Override
            public void onFailure(Call<List<Estatus>> call, Throwable t) {
                Toast.makeText(context, "Request failed", Toast.LENGTH_LONG).show();
            }

        });
    }

    public static void selectSpinnerItemByValue(Spinner spnr, long value) {
        Adapter adapter = spnr.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            Estatus estatus = (Estatus) adapter.getItem(position);
            if(estatus.getIdEstatus() == value) {
                spnr.setSelection(position);
                return;
            }
        }
    }

}