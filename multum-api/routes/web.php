<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuariosController;
use App\Http\Controllers\EntidadesController;
use App\Http\Controllers\TiposCuentasController;
use App\Http\Controllers\TiposEstatusController;
use App\Http\Controllers\EstatusController;
use App\Http\Controllers\CuentasController;
use App\Http\Controllers\MovimientosController;
use App\Http\Controllers\ConfiguracionController;
use Illuminate\Http\Request;

use App\Models\Movimiento;

// Main routes

Route::get('/', function () {
    $data = array(
        'info' => 'Laravel x PostgreSQL API for Multum App'
    );
    return response()->json($data);
});

// Usuarios

Route::get('/usuarios/{id_entidad}', [UsuariosController::class, 'getUsuarios']);

Route::post('/usuario', [UsuariosController::class, 'customSaveUsuario']);

Route::get('/usuario', [UsuariosController::class, 'getUsuario']);

// Entidades

Route::get('/entidades', [EntidadesController::class, 'getEntidades']);

Route::post('/entidad', [EntidadesController::class, 'customSaveEntidad']);

// TiposEstatus

Route::get('/tipos/estatus/{id_entidad}', [TiposEstatusController::class, 'getTiposEstatus']);

Route::post('/tipo/estatus', [TiposEstatusController::class, 'customSaveTipoEstatus']);

// Estatus

Route::get('/estatus/{id_entidad}', [EstatusController::class, 'getEstatus']);

Route::post('/estatus', [EstatusController::class, 'customSaveEstatus']);

// Tipos cuentas

Route::get('/tipos/cuentas/{id_entidad}', [TiposCuentasController::class, 'getTiposCuentas']);

Route::post('/tipo/cuenta', [TiposCuentasController::class, 'customSaveTipoCuenta']);

Route::delete('/tipo/cuenta', [TiposCuentasController::class, 'customDeleteTipoCuenta']);

// Cuentas

Route::get('/cuentas/{id_entidad}', [CuentasController::class, 'getCuentas']);

Route::post('/cuenta', [CuentasController::class, 'customSaveCuenta']);

Route::delete('/cuenta', [CuentasController::class, 'customDeleteCuenta']);

// Cuentas

Route::get('/movimientos/{id_entidad}', [MovimientosController::class, 'getMovimientos']);

Route::post('/movimiento', [MovimientosController::class, 'customSaveMovimiento']);

Route::delete('/movimiento', [MovimientosController::class, 'customDeleteCuenta']);

// Executive


Route::get('/consultas/{id_entidad}', function (Request $request) {
    date_default_timezone_set('America/Mexico_City');
    $date = new DateTime();
    $where = [
        ['tipo', '=', 'E'],
        ['fecha', '=', $date],
        ['id_entidad', '=', $request->id_entidad],
    ];
    $where2 = [
        ['tipo', '=', 'A'],
        ['fecha', '=', $date],
        ['id_entidad', '=', $request->id_entidad],
    ];
    $where3 = [
        ['tipo', '=', 'C'],
        ['fecha', '=', $date],
        ['id_entidad', '=', $request->id_entidad],
    ];    
    $data = array(
        'entradas' => floatval(Movimiento::where($where)->sum('importe')),
        'salidas' => floatval(Movimiento::where($where2)->sum('importe')) + floatval(Movimiento::where($where3)->sum("importe")),
        'date' => $date
    );
    return response()->json($data);
});

// Inicialización

Route::get('/configuracion/inicial', [ConfiguracionController::class, 'configuracionInicial']);

// Utils

Route::get('/token', function () {
    $data = array(
        'token' => csrf_token()
    );
    return response()->json($data);
});
