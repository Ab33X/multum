<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipoEstatus;

class TiposEstatusController extends Controller
{
    public function getTiposEstatus(Request $request, $id_entidad)
    {
        return TipoEstatus::where("id_entidad", $id_entidad)->get();
    }

    public function customSaveTipoEstatus(Request $request)
    {
        $tipo_estatus = new TipoEstatus;
        $tipo_estatus->usuario = $request->usuario;
        $tipo_estatus->tipo_estatus = $request->tipo_estatus;
        $tipo_estatus->id_entidad = $request->id_entidad;
        if ($tipo_estatus->save()) {
            $data = array(
                'status' => 'success',
                'message' => 'Tipo estatus guardado',
                'tipo_estatus' => $tipo_estatus
            );
        } else {
            $data = array(
                'status' => 'error',
                'message' => 'Tipo estatus no fue guardado'
            );
        }
        return response()->json($data);
    }
}
