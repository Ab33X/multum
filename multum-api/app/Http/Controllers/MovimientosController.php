<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movimiento;

class MovimientosController extends Controller
{
    public function getMovimientos(Request $request, $id_entidad)
    {
        return Movimiento::where("id_entidad", $id_entidad)->orderBy("fecha", "desc")->get();
    }

    public function customSaveMovimiento(Request $request)
    {
        if ($request->id_movimiento) {
            return $this->saveMovimiento($request);
        } else {
            return $this->insertMovimiento($request);
        }
    }

    public function insertMovimiento(Request $request)
    {
        $movimiento = new Movimiento;
        $movimiento->usuario = $request->usuario;
        $movimiento->id_entidad = $request->id_entidad;
        $movimiento->id_cuenta = $request->id_cuenta;
        $movimiento->fecha = $request->fecha;
        $movimiento->descripcion = $request->descripcion;
        $movimiento->importe = $request->importe;
        $movimiento->tipo = $request->tipo;
        if ($movimiento->save()) {
            $movimiento->status = "success";
            $movimiento->message = "Movimiento guardado";
        } else {
            $movimiento->status = "error";
            $movimiento->message = "Movimiento no fue guardado";
        }
        return response()->json($movimiento);
    }

    public function saveMovimiento(Request $request)
    {
        $movimiento = Movimiento::find($request->id_movimiento);
        $movimiento->usuario = $request->usuario;
        $movimiento->id_entidad = $request->id_entidad;
        $movimiento->id_cuenta = $request->id_cuenta;
        $movimiento->fecha = $request->fecha;
        $movimiento->descripcion = $request->descripcion;
        $movimiento->importe = $request->importe;
        $movimiento->tipo = $request->tipo;
        if ($movimiento->save()) {
            $movimiento->status = "success";
            $movimiento->message = "Movimiento guardado";
        } else {
            $movimiento->status = "error";
            $movimiento->message = "Movimiento no fue guardado";
        }
        return response()->json($movimiento);
    }

    public function customDeleteCuenta(Request $request)
    {
        $movimiento = Movimiento::find($request->id_movimiento);
        if ($movimiento->forceDelete()) {
            $movimiento->status = "success";
            $movimiento->message = "Movimiento fue eliminado";
        } else {
            $movimiento->status = "error";
            $movimiento->message = "Movimiento no fue eliminado";
        }
        return response()->json($movimiento);
    }
}
