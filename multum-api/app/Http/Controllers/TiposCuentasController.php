<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipoCuenta;

class TiposCuentasController extends Controller
{
    public function getTiposCuentas(Request $request, $id_entidad)
    {
        return TipoCuenta::where("id_entidad", $id_entidad)->orderBy("clave")->get();
    }

    public function customSaveTipoCuenta(Request $request){
        if($request->id_tipo_cuenta){
            return $this->saveTipoCuenta($request);
        }else {
            return $this->insertTipoCuenta($request);
        }
    }

    public function insertTipoCuenta(Request $request)
    {
        $tipocuenta = new TipoCuenta;
        $tipocuenta->usuario = $request->usuario;
        $tipocuenta->tipo_cuenta = $request->tipo_cuenta;
        $tipocuenta->clave = $request->clave;
        $tipocuenta->id_estatus = $request->id_estatus;
        $tipocuenta->id_entidad = $request->id_entidad;
        if ($tipocuenta->save()) {
            $tipocuenta->status = "success";
            $tipocuenta->message = "Tipo cuenta guardado";
        } else {
            $tipocuenta->status = "error";
            $tipocuenta->message = "Tipo cuenta no fue guardado";
        }
        return response()->json($tipocuenta);
    }

    public function saveTipoCuenta(Request $request)
    {
        $tipocuenta = TipoCuenta::find($request->id_tipo_cuenta);
        $tipocuenta->usuario = $request->usuario;
        $tipocuenta->tipo_cuenta = $request->tipo_cuenta;
        $tipocuenta->clave = $request->clave;
        $tipocuenta->id_estatus = $request->id_estatus;
        $tipocuenta->id_entidad = $request->id_entidad;
        if ($tipocuenta->save()) {
            $tipocuenta->status = "success";
            $tipocuenta->message = "Tipo cuenta guardado";
        } else {
            $tipocuenta->status = "error";
            $tipocuenta->message = "Tipo cuenta no fue guardado";
        }
        return response()->json($tipocuenta);
    }

    public function customDeleteTipoCuenta(Request $request){
        $tipocuenta = TipoCuenta::find($request->id_tipo_cuenta);
        if($tipocuenta->forceDelete()){            
            $tipocuenta->status = "success";
            $tipocuenta->message = "Tipo cuenta fue eliminado";
        } else {
            $tipocuenta->status = "error";
            $tipocuenta->message = "Tipo cuenta no fue eliminado";
        }
        return response()->json($tipocuenta);
    }

}
