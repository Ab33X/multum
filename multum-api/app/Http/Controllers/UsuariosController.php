<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use Illuminate\Support\Facades\Hash;

class UsuariosController extends Controller
{
    public function getUsuarios(Request $request, $id_entidad)
    {
        return Usuario::where("id_entidad", $id_entidad)->get();
    }

    public function getUsuario(Request $request)
    {
        $pusuario = $request->usuario;
        $ppassword = $request->password;
        $usuario = Usuario::where("usuario", $pusuario)->first();
        if($usuario){
            if (Hash::check($ppassword, $usuario->password)) {
                $usuario->status = "success";
                $usuario->message = "Bienvenido";
                $usuario->token = csrf_token();
            } else {
                $usuario->status = "error";
                $usuario->message = "Usuario o contraseña inválidos";
            }
        } else {
            $usuario = array(
                'status' => 'error',
                'message' => 'El usuario ('.$pusuario.') no existe.'
            );
        }

        return response()->json([$usuario]);
    }

    public function customSaveUsuario(Request $request)
    {
        $usuario = new Usuario;
        $usuario->usuario = $request->usuario;
        $usuario->email = $request->email;
        $usuario->nombre = $request->nombre;
        $usuario->password = Hash::make($request->password);
        $usuario->super = $request->super;
        $usuario->id_entidad = $request->id_entidad;
        if ($usuario->save()) {
            $data = array(
                'status' => 'success',
                'message' => 'Usuario guardado',
                'usuario' => $usuario
            );
        } else {
            $data = array(
                'status' => 'error',
                'message' => 'Usuario no fue guardado'
            );
        }
        return response()->json($data);
    }
}
