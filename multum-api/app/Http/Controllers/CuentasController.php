<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cuenta;

class CuentasController extends Controller
{
    public function getCuentas(Request $request, $id_entidad)
    {
        return Cuenta::where("id_entidad", $id_entidad)->orderBy("orden")->get();
    }

    public function customSaveCuenta(Request $request)
    {

        if ($request->id_cuenta) {
            return $this->saveCuenta($request);
        } else {
            return $this->insertCuenta($request);
        }
    }

    public function insertCuenta(Request $request)
    {
        $cuenta = new Cuenta;
        $cuenta->usuario = $request->usuario;
        $cuenta->cuenta = $request->cuenta;
        $cuenta->clave = $request->clave;
        $cuenta->orden = $request->orden;
        $cuenta->id_estatus = $request->id_estatus;
        $cuenta->id_tipo_cuenta = $request->id_tipo_cuenta;
        $cuenta->id_entidad = $request->id_entidad;
        if ($cuenta->save()) {
            $cuenta->status = "success";
            $cuenta->message = "Cuenta guardado";
        } else {
            $cuenta->status = "error";
            $cuenta->message = "Cuenta no fue guardado";
        }
        return response()->json($cuenta);
    }

    public function saveCuenta(Request $request)
    {
        $cuenta = Cuenta::find($request->id_cuenta);
        $cuenta->usuario = $request->usuario;
        $cuenta->cuenta = $request->cuenta;
        $cuenta->clave = $request->clave;
        $cuenta->orden = $request->orden;
        $cuenta->id_estatus = $request->id_estatus;
        $cuenta->id_tipo_cuenta = $request->id_tipo_cuenta;
        $cuenta->id_entidad = $request->id_entidad;
        if ($cuenta->save()) {
            $cuenta->status = "success";
            $cuenta->message = "Cuenta guardado";
        } else {
            $cuenta->status = "error";
            $cuenta->message = "Cuenta no fue guardado";
        }
        return response()->json($cuenta);
    }

    public function customDeleteCuenta(Request $request)
    {
        $cuenta = Cuenta::find($request->id_cuenta);
        if ($cuenta->forceDelete()) {
            $cuenta->status = "success";
            $cuenta->message = "Cuenta fue eliminado";
        } else {
            $cuenta->status = "error";
            $cuenta->message = "Cuenta no fue eliminado";
        }
        return response()->json($cuenta);
    }
}
