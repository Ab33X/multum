<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Entidad;

class EntidadesController extends Controller
{
    public function getEntidades(Request $request){
        return Entidad::paginate(5);
    }

    public function customSaveEntidad(Request $request){
        $entidad = new Entidad;
        $entidad->clave = $request->clave;
        $entidad->entidad = $request->entidad;
        $entidad->telefono = $request->telefono;
        $entidad->direccion = $request->direccion;
        $entidad->codigo_postal = $request->codigo_postal;
        if ($entidad->save()) {
            $data = array(
                'status' => 'success',
                'message' => 'Entidad guardada',
                'entidad' => $entidad
            );
        } else {
            $data = array(
                'status' => 'error',
                'message' => 'Entidad no fue guardada'
            );
        }
        return response()->json($data);
    }
}
