<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estatus;

class EstatusController extends Controller
{
    public function getEstatus(Request $request, $id_entidad)
    {
        return Estatus::where("id_entidad", $id_entidad)->get();
    }

    public function customSaveEstatus(Request $request)
    {
        $estatus = new Estatus;
        $estatus->usuario = $request->usuario;
        $estatus->estatus = $request->estatus;
        $estatus->id_tipo_estatus = $request->id_tipo_estatus;
        $estatus->id_entidad = $request->id_entidad;
        if ($estatus->save()) {
            $data = array(
                'status' => 'success',
                'message' => 'Estatus guardado',
                'estatus' => $estatus
            );
        } else {
            $data = array(
                'status' => 'error',
                'message' => 'Estatus no fue guardado'
            );
        }
        return response()->json($data);
    }
}
