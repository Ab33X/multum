<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Entidad;
use App\Models\Usuario;
use Illuminate\Support\Facades\Hash;
use App\Models\TipoEstatus;
use App\Models\Estatus;

class ConfiguracionController extends Controller
{
    public function configuracionInicial(Request $request)
    {
        $count = Entidad::count("id_entidad");
        $obj = (object) array(
            'STATE' => 'Processed'
        );
        if($count == 0){
            $entidad = new Entidad();
            $entidad->clave = 'BR';
            $entidad->entidad = 'Barbacoa Reyes';
            $entidad->telefono = '9933600549';
            $entidad->direccion = 'Congreso de chilpancingo L5, Olmeca, Villahermosa, Tabasco, México.';
            $entidad->codigo_postal = '86019';
            if ($entidad->save()) {
                $obj->message = "Entidad configurada";
            } else {
                $obj->message = "Entidad no fue configurado";
            }
    
            $usuario = new Usuario;
            $usuario->usuario = 'admin';
            $usuario->email = 'admin@admin.com';
            $usuario->nombre = 'Administrador multum';
            $usuario->password = Hash::make('multum');
            $usuario->super = '1';
            $usuario->id_entidad = $entidad->id_entidad;
            if ($usuario->save()) {
                $obj->message2 = 'Usuario administrador configurado';
                $usuario->raw_password = 'multum';
                $obj->usuario = $usuario;
            } else {
                $obj->message2 = 'Usuario administrador no fue configurado';
            }
    
            $tipo_estatus = new TipoEstatus();
            $tipo_estatus->id_entidad = $entidad->id_entidad;
            $tipo_estatus->usuario = $usuario->usuario;
            $tipo_estatus->tipo_estatus = 'Base';
            if ($tipo_estatus->save()) {
                $obj->message3 = 'Usuario administrador configurado';
            } else {
                $obj->message3 = 'Usuario administrador no fue configurado';
            }
    
            $estatus1 = new Estatus();
            $estatus1->id_entidad = $entidad->id_entidad;
            $estatus1->usuario = $usuario->usuario;
            $estatus1->id_tipo_estatus = $tipo_estatus->id_tipo_estatus;
            $estatus1->estatus = 'Activo';
            if ($estatus1->save()) {
                $obj->message4 = 'Estatus 1 configurado';
            } else {
                $obj->message4 = 'Estatus 1 no fue configurado';
            }
    
            $estatus2 = new Estatus();
            $estatus2->id_entidad = $entidad->id_entidad;
            $estatus2->usuario = $usuario->usuario;
            $estatus2->id_tipo_estatus = $tipo_estatus->id_tipo_estatus;
            $estatus2->estatus = 'Inactivo';
            if ($estatus2->save()) {
                $obj->message5 = 'Estatus 2 configurado';
            } else {
                $obj->message5 = 'Estatus 2 no fue configurado';
            }
        } else {
            $obj->message = "Ya se ha realizado la configuracion inicial anteriormente";
            $obj->usuario = Usuario::where('usuario', 'admin')->get();
        }

        return response()->json($obj);
    }
}
