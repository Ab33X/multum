<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoEstatus extends Model
{
    use HasFactory;
    protected $table = "tipos_estatus";
    protected $primaryKey = "id_tipo_estatus";

    public function estatus()
    {
        return $this->hasMany(Estatus::class, "id_tipo_estatus", "id_tipo_estatus");
    }

    public function entidad()
    {
        return $this->belongsTo(Entidad::class, "id_entidad", "id_entidad");
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, "usuario", "usuario");
    }
}
