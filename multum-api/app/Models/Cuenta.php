<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    use HasFactory;
    protected $table = "cuentas";
    protected $primaryKey = "id_cuenta";
    protected $with = ['estatus', 'tipoCuenta'];

    public function estatus()
    {
        return $this->belongsTo(Estatus::class, "id_estatus", "id_estatus");
    }

    public function tipoCuenta()
    {
        return $this->belongsTo(TipoCuenta::class, "id_tipo_cuenta", "id_tipo_cuenta");
    }
}
