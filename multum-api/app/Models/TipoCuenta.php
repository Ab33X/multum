<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoCuenta extends Model
{
    use HasFactory;

    protected $table = "tipos_cuentas";
    protected $primaryKey = "id_tipo_cuenta";
    protected $with = ['estatus'];

    public function estatus()
    {
        return $this->belongsTo(Estatus::class, "id_estatus", "id_estatus");
    }

    public function entidad()
    {
        return $this->belongsTo(Entidad::class, "id_entidad", "id_entidad");
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, "usuario", "usuario");
    }
}
