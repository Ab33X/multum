<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Usuario;

class Entidad extends Model
{
    use HasFactory;

    protected $table = "entidades";
    protected $primaryKey = "id_entidad";

    public function usuarios(){
        return $this->hasMany(Usuario::class, "id_entidad", "id_entidad");
    }
}
