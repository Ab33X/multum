<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    use HasFactory;
    protected $table = "movimientos";
    protected $primaryKey = "id_movimiento";
    protected $with = ['cuenta'];

    public function cuenta()
    {
        return $this->belongsTo(Cuenta::class, "id_cuenta", "id_cuenta");
    }
}
