<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estatus extends Model
{
    use HasFactory;
    protected $primaryKey = "id_estatus";
    protected $table = "estatus";
    protected $with = [ 'tipoEstatus' ];

    public function tipoEstatus(){
        return $this->belongsTo(TipoEstatus::class, "id_tipo_estatus", "id_tipo_estatus");
    }
    
    public function entidad(){
        return $this->belongsTo(Entidad::class, "id_entidad", "id_entidad");
    }

    public function usuario(){
        return $this->belongsTo(Usuario::class, "usuario", "usuario");
    }
}
