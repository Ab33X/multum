<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Entidad;

class Usuario extends Model
{
    use HasFactory;

    protected $table = "usuarios";
    protected $primaryKey = "id_usuario";
    protected $with = ['entidad'];

    public function entidad(){
        return $this->belongsTo(Entidad::class, "id_entidad", "id_entidad");
    }
}
