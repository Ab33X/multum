<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_cuentas', function (Blueprint $table) {
            $table->id("id_tipo_cuenta");
            $table->text("usuario")->references('usuario')->on('usuarios')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger("id_entidad");
            $table->foreign('id_entidad', 'fk_tipo_cuenta_entidad')->references('id_entidad')->on('entidades')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger("id_estatus");
            $table->foreign('id_estatus', 'fk_tipo_cuenta_estatus')->references('id_estatus')->on('estatus')->onDelete('restrict')->onUpdate('restrict');
            $table->text("clave");
            $table->text("tipo_cuenta");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_cuentas');
    }
}
