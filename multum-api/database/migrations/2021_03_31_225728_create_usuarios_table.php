<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id("id_usuario");
            $table->text("usuario");
            $table->text("email");
            $table->text("nombre");
            $table->text("password");
            $table->integer("super");
            $table->unsignedBigInteger("id_entidad");
            $table->foreign('id_entidad', 'fk_usuario_entidad')->references('id_entidad')->on('entidades')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
