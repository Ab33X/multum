<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoEstatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_estatus', function (Blueprint $table) {
            $table->id("id_tipo_estatus");
            $table->text("usuario")->references('usuario')->on('usuarios')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger("id_entidad");
            $table->foreign('id_entidad', 'fk_tipo_estatus_entidad')->references('id_entidad')->on('entidades')->onDelete('restrict')->onUpdate('restrict');
            $table->text("tipo_estatus");            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_estatus');
    }
}
