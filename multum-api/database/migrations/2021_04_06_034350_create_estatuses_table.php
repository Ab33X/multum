<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estatus', function (Blueprint $table) {
            $table->id("id_estatus");
            $table->text("usuario")->references('usuario')->on('usuarios')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger("id_entidad");
            $table->foreign('id_entidad', 'fk_estatus_entidad')->references('id_entidad')->on('entidades')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger("id_tipo_estatus");
            $table->foreign('id_tipo_estatus', 'fk_tipo_estatus')->references('id_tipo_estatus')->on('tipos_estatus')->onDelete('restrict')->onUpdate('restrict');
            $table->text("estatus");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estatus');
    }
}
