<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas', function (Blueprint $table) {
            $table->id("id_cuenta");            
            $table->text("usuario")->references('usuario')->on('usuarios')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger("id_entidad");
            $table->foreign('id_entidad', 'fk_entidad_cuenta')->references('id_entidad')->on('entidades')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger("id_estatus");
            $table->foreign('id_estatus', 'fk_estatus_cuenta')->references('id_estatus')->on('estatus')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger("id_tipo_cuenta");
            $table->foreign('id_tipo_cuenta', 'fk_tipo_cuenta')->references('id_tipo_cuenta')->on('tipos_cuentas')->onDelete('restrict')->onUpdate('restrict');
            $table->text("clave");
            $table->text("cuenta");
            $table->integer("orden");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas');
    }
}
