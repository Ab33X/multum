<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos', function (Blueprint $table) {
            $table->id("id_movimiento");          
            $table->text("usuario")->references('usuario')->on('usuarios')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger("id_entidad");
            $table->foreign('id_entidad', 'fk_entidad_movimiento')->references('id_entidad')->on('entidades')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger("id_cuenta");
            $table->foreign('id_cuenta', 'fk_cuenta_movimiento')->references('id_cuenta')->on('cuentas')->onDelete('restrict')->onUpdate('restrict');
            $table->date("fecha");
            $table->text("descripcion");
            $table->double("importe", 12, 2);
            $table->char("tipo", 1); //Cargo C ó abono A ó entrada E
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos');
    }
}
